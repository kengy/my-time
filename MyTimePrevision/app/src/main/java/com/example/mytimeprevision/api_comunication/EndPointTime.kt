package com.example.mytimeprevision.api_comunication


import com.example.mytimeprevision.model.myTimeRespnse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface EndPointTime {


	//weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22
	@GET("weather?")
fun endGetTime (@Path ("name")  name: String): Call<myTimeRespnse>


	@GET("data/2.5/weather?")
	fun getCurrentWeatherData(@Query("lat") lat: String, @Query("lon") lon: String, @Query("APPID") app_id: String): Call<myTimeRespnse>
}