package com.example.mytimeprevision.api_comunication

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NetWorkUtils {

	fun getRetrofitInstance (path: String) : Retrofit {

		return Retrofit.Builder().
				baseUrl(path).
			addConverterFactory(GsonConverterFactory.create()).
			build()
	}
}